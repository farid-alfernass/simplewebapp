# User Service
This project written in javascript based on a clean architecture that represents DDD and CQRS patterns. 

## Project Structure
- [ ] `bin/`
  - [ ] `app/`: serve routing endpoint.
  - [ ] `auth/`: contains auth middleware.
  - [ ] `helpers/`
    - [ ] `databases/`: contains database configurations and commands.
    - [ ] `error/`: contains custom http error messages.
    - [ ] `http-status/`: contains custom http status codes.
    - [ ] `utils/`: contains utility library.
  - [ ] `infra/`
    - [ ] `config/`: define the app configuration.
  - [ ] `modules/`: define the core domain.
    - [ ] `events/`: user subdomain.
      - [ ] `handlers/`: defines call handlers from repositories.
      - [ ] `repositories/`: contains user commands and queries.
      - [ ] `utils/`: contains domain utils.
- [ ] `docs/`: contains api documentation
  - [ ] `postman/`: consist postman collection
  - [ ] `swagger/`: consist swagger docs
- [ ] `test/`: contains testing purpose.
  - [ ] `integration/`
  - [ ] `unit/`

## Getting Started
### Prerequisites

What things you need to install to run this project:

```
- Node.js v20
- Node Package Manager v10
- MongoDB
- Postgres
- Elastic APM
- Kafka
- RedisDB
```

**Note:** Elastic APM needs Elasticsearch and Kibana installed on your machine.

### Quick Start
To run this project, make sure that all prerequisites above are installed on your machine. Steps on how to initialize and run this project are as follows:

1. Clone this repo to your local machine

2. Copy env from [COPY HERE](https://gitlab.com/-/snippets/3696335) and save .env

3. Install dependencies:
   ```
   $ npm install
   ```

4. Start the server:
   ```
   $ npm run start
   ```

### Running the tests

Just simply use this command to run the automated tests:
```
$ npm run test
```

Get coverage value for this system:
```
$ npm run cover
```

### Check coding style with lint analysis ###
It is encouraged to run lint analysis before push your code to make sure that there are no issues on your coding style / formatting
```
$ npm run lint
```

To fix simple error code format, run this command
```
$ npm run lint:fix
```

### High Level Design Architecture
![High-Level Design Architecture](High-level-architecture.jpeg)

### Low Level Design Architecture

### ERD Document


### Author
* [Farid Tri Wicaksono](https://github.com/farid-alfernass)

### Built With

* [Restify] The rest framework used
* [Npm] - Dependency Management
* [Docker] - Container Management
* [Kafka] - Message broker
* [ELK] - APM Monitoring