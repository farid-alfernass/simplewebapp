const config = require('../infra/configs/global_config');
const jinbei = require('jinbei');
if (config.get('/monitoring') !== 0) {
  jinbei.apm.init();
}
const healtCheck = require('./health_check');
const restify = require('restify');
const corsMiddleware = require('restify-cors-middleware2');
const project = require('../../package.json');
const routes = require('../routes');
const mongoConnectionPooling = require('../helpers/databases/mongodb/connection');
const observers = require('../modules/observers');
const mongoConfig = config.get('/mongoDbUrl');
class AppServer {

  constructor() {

    this.server = restify.createServer({
      name: `${project.name}-server`,
      version: project.version
    });
    jinbei.init(this.server);
    jinbei.initLogger();
    this.server.serverKey = '';
    this.server.use(restify.plugins.acceptParser(this.server.acceptable));
    this.server.use(restify.plugins.queryParser());
    this.server.use(restify.plugins.bodyParser());
    this.server.use(restify.plugins.authorizationParser());

    // required for CORS configuration
    const corsConfig = corsMiddleware({
      preflightMaxAge: 5,
      origins: ['*'],
      // ['*'] -> to expose all header, any type header will be allow to access
      // X-Requested-With,content-type,GET, POST, PUT, PATCH, DELETE, OPTIONS -> header type
      allowHeaders: ['Authorization'],
      exposeHeaders: ['Authorization']
    });
    this.server.pre(corsConfig.preflight);
    this.server.use(corsConfig.actual);
    // this.server.use(morgan('combined', { stream: logger.stream }));

    this.server.use(jinbei.initBasicAuth());
    this.server.use(jinbei.initLogger());

    this.server.get('/', (req, res, next) => {
      res.send(200, { success: true, data: 'server init', message: 'This service is running properly', code: 200 });
      next();
    });

    this.server.get('/user/health', (req, res, next) => {
      healtCheck.checkServiceHealth(this.server);
      res.send(200, { success: true, data: 'server init', message: 'This service is running health check', code: 200 });
      next();
    });

    routes(this.server);

    mongoConnectionPooling.init(mongoConfig);
    observers.init();
    // jinbei.RedisConnection.init(config.get('/redis').connection);
  }
}

module.exports = AppServer;
