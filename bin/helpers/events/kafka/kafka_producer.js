const kafka = require('kafka-node');
const jinbei = require('jinbei');
const config = require('../../../infra/configs/global_config');
const client = new kafka.KafkaClient(config.get('/kafka'));
const producer = new kafka.HighLevelProducer(client);
const ctx = 'kafka-producer';

producer.on('ready', () => {
  jinbei.log(ctx, 'ready', 'Kafka Producer is connected and ready.');
});

const kafkaSendProducer = (data) => {
  const buffer = new Buffer.from(JSON.stringify(data.body));
  const record = [
    {
      topic: data.topic,
      messages: buffer,
      attributes: data.attributes,
      partitionerType: data.partition
    }
  ];
  producer.send(record, (err, data) => {
    if(err) {
      jinbei.log(ctx, 'error', 'producer-error-send');
    }
    jinbei.log(ctx, `Send data to ${JSON.stringify(data)}`, 'Data has been send');
  });
};


producer.on('error', async (error) => {
  jinbei.log(ctx, error, 'Kafka Producer Error');
});

module.exports = {
  kafkaSendProducer
};
