const nodemailer = require('nodemailer');
const ctx = 'utils-mailHandler';
const jinbei = require('jinbei');
const wrapper = jinbei.Wrapper;
const config = require('../../infra/configs/global_config');
const { ExpectationFailedError } = jinbei.Error;
const mailConfig = config.get('/mail');

const transport = nodemailer.createTransport({
  host: mailConfig.host,
  port: mailConfig.port,
  auth: {
    user: mailConfig.user,
    pass: mailConfig.pass,
  }
});

module.exports.send = async (html, emailUser, emailSubject) => {
  const mailOptions = {
    from: `${mailConfig.sender}`,
    to: emailUser,
    subject: emailSubject,
    html,
    text: html
  };
  try {
    return await transport.sendMail(mailOptions);
  } catch (err) {
    jinbei.log(ctx, err, 'Fail to send the message');
    return wrapper.error(new ExpectationFailedError('Fail to send the message'));
  }
};
