const jinbei = require('jinbei');
const userHandler = require('../modules/user/handlers/api_handler');

module.exports = (server) => {
  server.post('/users/v1/login', jinbei.isAuthenticated, userHandler.loginUser);
  server.get('/users/v1/profile', jinbei.verifyToken, userHandler.getUser);
  server.post('/users/v1/register', jinbei.isAuthenticated, userHandler.registerUser);
  server.post('/users/v1/refresh-token', jinbei.isAuthenticated, userHandler.getRefreshToken);
  server.post('/users/v1/logout', jinbei.isAuthenticated, userHandler.logoutUser);
  server.post('/users/v1/verify', jinbei.isAuthenticated, userHandler.verifyOtp);
};
