
const Query = require('./query');
const jinbei = require('jinbei');
const wrapper = jinbei.Wrapper;
const { NotFoundError } = jinbei.Error;

class User {

  constructor(db){
    this.query = new Query(db);
  }

  async viewUser(userId) {
    const user = await this.query.findOneUser({userId:userId});
    if (user.err) {
      return wrapper.error(new NotFoundError('Can not find user'));
    }
    const { data } = user;
    delete data._id;
    delete data.password;
    return wrapper.data(data);
  }

}

module.exports = User;
