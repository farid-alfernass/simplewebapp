const userEventHandler = require('./user/handlers/event_handler');
const jinbei = require('jinbei');

const init = () => {
  jinbei.log('info','Observer is Running...','myEmitter.init');
  initEventListener();
};
const initEventListener = () => {
  userEventHandler.sendEmailOtp();
  // userEventHandler.sendSmsOtp();
};

module.exports = {
  init: init
};
