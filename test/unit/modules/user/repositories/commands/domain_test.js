const assert = require('assert');
const sinon = require('sinon');

const command = require('../../../../../../bin/modules/user/repositories/commands/command');
const query = require('../../../../../../bin/modules/user/repositories/queries/query');
const jinbei = require('jinbei');
const User = require('../../../../../../bin/modules/user/repositories/commands/domain');
const commonUtil = require('../../../../../../bin/helpers/utils/common');
const common = require('../../../../../../bin/modules/user/utils/common');
const mailHanlder = require('../../../../../../bin/helpers/utils/mail_handler');
const redis = jinbei.RedisDB;

describe('User-domain', () => {

  const queryResult = {
    'err': null,
    'data': {
      '_id': '5bac53b45ea76b1e9bd58e1c',
      'email': 'email@gmail.com',
      'password': '3d3811045545be3a9e91e2352f9c668a:50aa9b313ef3365801335297c09c13f0',
      'isConfirmed': true
    },
    'message': 'Your Request Has Been Processed',
    'code': 200
  };

  const payload = {
    'username': 'email@gmail.com',
    'password': 'assessment123'
  };

  const db = {
    setCollection: sinon.stub()
  };

  const user = new User(db);

  const accessToken = 'eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9';
  const refreshToken = 'eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9';

  before(() => {
    sinon.stub(jinbei, 'log');
  });

  after(() => {
    jinbei.log.restore();
  });

  describe('generateCredential', () => {

    it('should generate jwt token', async() => {
      sinon.stub(query.prototype, 'findOneUser').resolves(queryResult);
      sinon.stub(commonUtil, 'decryptWithIV').returns(payload.password);
      sinon.stub(jinbei, 'generateToken').resolves(accessToken);
      sinon.stub(jinbei, 'generateRefreshToken').resolves(refreshToken);

      const res = await user.generateCredential(payload);
      assert.notEqual(res, null);
      assert.deepEqual(res, {
        data: {
          accessToken,
          refreshToken
        },
        err: null
      });
      query.prototype.findOneUser.restore();
      commonUtil.decryptWithIV.restore();
      jinbei.generateToken.restore();
      jinbei.generateRefreshToken.restore();
    });

    it('should return error', async() => {
      sinon.stub(query.prototype, 'findOneUser').resolves({ err: 'err'});

      const res = await user.generateCredential(payload);
      assert.notEqual(res.err, null);

      query.prototype.findOneUser.restore();

    });

    it('should return user invalid', async() => {
      const payload = {
        'username': 'email@gmail.com',
        'password': 'assessment123'
      };
      delete queryResult.data.isConfirmed;
      sinon.stub(query.prototype, 'findOneUser').resolves(queryResult);
      sinon.stub(commonUtil, 'decryptWithIV').returns(queryResult.password);

      const res = await user.generateCredential(payload);
      assert.notEqual(res.err, null);

      commonUtil.decryptWithIV.restore();
      query.prototype.findOneUser.restore();
    });
  });

  describe('deleteCredential', () => {

    it('should error verify jwt token', async() => {
      sinon.stub(jinbei, 'verifyAccessToken').resolves({err:{}, data:null});

      const res = await user.deleteCredential(payload);
      assert.notEqual(res, null);
      assert.deepEqual(res, {
        data: null,
        err: {}
      });
      jinbei.verifyAccessToken.restore();
    });

    it('should error success delete credential', async() => {
      sinon.stub(jinbei, 'verifyAccessToken').resolves({err:null, data:{}});
      sinon.stub(redis.prototype, 'deleteKey').resolves({});
      sinon.stub(redis.prototype, 'setDataEx').resolves({});

      const res = await user.deleteCredential(payload);
      assert.notEqual(res, null);
      assert.deepEqual(res, {
        data: 'success',
        err: null
      });
      jinbei.verifyAccessToken.restore();
      redis.prototype.deleteKey.restore();
      redis.prototype.setDataEx.restore();
    });

  });

  describe('verifyOtp', () => {

    it('should error otp not found', async() => {
      sinon.stub(redis.prototype, 'getData').resolves({err:{}, data:null});

      const res = await user.verifyOtp(payload);
      assert.notEqual(res, null);
      assert.deepEqual(res, {
        data: null,
        err: {
          code: undefined,
          data: undefined,
          message: 'user\'s otp not found'
        }
      });
      redis.prototype.getData.restore();
    });

    it('should error otp not match', async() => {
      payload.otp = '123456';
      sinon.stub(redis.prototype, 'getData').resolves({err:null, data:'{"data":"12345"}'});

      const res = await user.verifyOtp(payload);
      assert.notEqual(res, null);
      assert.deepEqual(res, {
        data: null,
        err: {
          code: undefined,
          data: undefined,
          message: 'user\'s otp doesn\'t match'
        }
      });
      redis.prototype.getData.restore();
    });

    it('should success verify', async() => {
      payload.otp = '12345';
      sinon.stub(redis.prototype, 'getData').resolves({err:null, data:'{"data":"12345"}'});
      sinon.stub(common, 'filterEmailOrMobileNumber').resolves({email:payload.username});
      sinon.stub(query.prototype, 'findOneUser').resolves({ err:null,data: {}});
      sinon.stub(command.prototype, 'upsertOneUser').resolves({ err:null,data: null});
      sinon.stub(redis.prototype, 'deleteKey').resolves({});
      const res = await user.verifyOtp(payload);
      assert.notEqual(res, null);
      assert.deepEqual(res, {
        data: 'success',
        err: null
      });
      redis.prototype.getData.restore();
      common.filterEmailOrMobileNumber.restore();
      query.prototype.findOneUser.restore();
      command.prototype.upsertOneUser.restore();
      redis.prototype.deleteKey.restore();

    });

    it('should error user not found', async() => {
      payload.otp = '12345';
      sinon.stub(redis.prototype, 'getData').resolves({err:null, data:'{"data":"12345"}'});
      sinon.stub(common, 'filterEmailOrMobileNumber').resolves({email:payload.username});
      sinon.stub(query.prototype, 'findOneUser').resolves({ err:{},data: null});

      const res = await user.verifyOtp(payload);
      assert.notEqual(res, null);
      assert.deepEqual(res, {
        data: null,
        err: {
          code: undefined,
          data: undefined,
          message: 'user not found'
        }
      });
      redis.prototype.getData.restore();
      common.filterEmailOrMobileNumber.restore();
      query.prototype.findOneUser.restore();

    });

  });

  describe('register', () => {

    it('should success register', async() => {
      sinon.stub(query.prototype, 'findOneUser').resolves({ data: null});
      sinon.stub(command.prototype, 'insertOneUser').resolves(queryResult);
      sinon.stub(commonUtil, 'encryptWithIV').resolves(queryResult);

      const res = await user.registerUser(payload);

      assert.equal(res.data.email, 'email@gmail.com');
      commonUtil.encryptWithIV.restore();
      query.prototype.findOneUser.restore();
      command.prototype.insertOneUser.restore();
    });

    it('should return error', async() => {
      sinon.stub(common, 'filterEmailOrMobileNumber').resolves({email: 'email@gmail.com'});

      sinon.stub(query.prototype, 'findOneUser').resolves(queryResult);

      const res = await user.registerUser(payload);
      assert.notEqual(res.err, null);
      common.filterEmailOrMobileNumber.restore();
      query.prototype.findOneUser.restore();
    });
  });

  describe('getRefreshToken', () => {

    it('should generate new jwt token', async() => {
      sinon.stub(jinbei, 'verifyRefreshToken').returns({data: {userId: '5bac53b45ea76b1e9bd58e1c'}});
      sinon.stub(jinbei, 'generateToken').resolves(accessToken);
      sinon.stub(jinbei, 'generateRefreshToken').resolves(refreshToken);

      const res = await user.getRefreshToken({
        refreshToken: 'eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9'
      });
      assert.deepEqual(res.data, {
        accessToken,
        refreshToken
      });

      jinbei.verifyRefreshToken.restore();
      jinbei.generateToken.restore();
      jinbei.generateRefreshToken.restore();
    });

    it('should return error', async() => {
      sinon.stub(jinbei, 'verifyRefreshToken').returns({err: 'err'});

      const res = await user.getRefreshToken({
        refreshToken: 'eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9'
      });
      assert.notEqual(res.err, null);

      jinbei.verifyRefreshToken.restore();
    });
  });

  describe('sendEmailOtp', () => {
    const params = {
      email: 'email@gmail.com',
      otp: '123456'
    };
    it('should send otp', async() => {
      sinon.stub(mailHanlder, 'send').returns({});
      sinon.stub(redis.prototype, 'setDataEx').resolves({});

      const res = await user.sendEmailOtp(params);
      assert.deepEqual(res, {
        data: 'success',
        err: null
      });

      mailHanlder.send.restore();
      redis.prototype.setDataEx.restore();
    });

  });


});
