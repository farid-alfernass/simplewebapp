const commandHandler = require('../../../../../../bin/modules/user/repositories/commands/command_handler');
const User = require('../../../../../../bin/modules/user/repositories/commands/domain');
const sinon = require('sinon');
const assert = require('assert');

describe('User-commandHandler', () => {

  const data = {
    success: true,
    data: 'eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9',
    message: 'Your Request Has Been Processed',
    code: 200
  };

  const payload = {
    'username': 'alifsn',
    'password': 'telkomdev'
  };

  describe('postDataLogin', () => {

    it('should return access token', async() => {
      sinon.stub(User.prototype, 'generateCredential').resolves(data);

      const rs = await commandHandler.loginUser(payload);

      assert.notEqual(rs.data, null);
      assert.equal(rs.code, 200);

      User.prototype.generateCredential.restore();
    });
  });

  describe('register', () => {

    it('should info success register', async() => {
      sinon.stub(User.prototype, 'registerUser').resolves(data);

      const rs = await commandHandler.registerUser(payload);

      assert.notEqual(rs.data, null);
      assert.equal(rs.code, 200);

      User.prototype.registerUser.restore();
    });
  });

  describe('logoutUser', () => {

    it('should info success logout', async() => {
      sinon.stub(User.prototype, 'deleteCredential').resolves(data);

      const rs = await commandHandler.logoutUser(payload);

      assert.notEqual(rs.data, null);
      assert.equal(rs.code, 200);

      User.prototype.deleteCredential.restore();
    });
  });

  describe('getRefreshToken', () => {

    it('should info success get Refresh Token', async() => {
      sinon.stub(User.prototype, 'getRefreshToken').resolves(data);

      const rs = await commandHandler.getRefreshToken(payload);

      assert.notEqual(rs.data, null);
      assert.equal(rs.code, 200);

      User.prototype.getRefreshToken.restore();
    });
  });

  describe('verifyOtp', () => {

    it('should info success verify Otp', async() => {
      sinon.stub(User.prototype, 'verifyOtp').resolves(data);

      const rs = await commandHandler.verifyOtp(payload);

      assert.notEqual(rs.data, null);
      assert.equal(rs.code, 200);

      User.prototype.verifyOtp.restore();
    });
  });

  describe('sendEmailOtp', () => {

    it('should info success send Email Otp', async() => {
      sinon.stub(User.prototype, 'sendEmailOtp').resolves(data);

      const rs = await commandHandler.sendEmailOtp(payload);

      assert.notEqual(rs.data, null);
      assert.equal(rs.code, 200);

      User.prototype.sendEmailOtp.restore();
    });
  });

  describe('sendSmsOtp', () => {

    it('should info success send Sms Otp', async() => {
      sinon.stub(User.prototype, 'sendSmsOtp').resolves(data);

      const rs = await commandHandler.sendSmsOtp(payload);

      assert.notEqual(rs.data, null);
      assert.equal(rs.code, 200);

      User.prototype.sendSmsOtp.restore();
    });
  });

});
