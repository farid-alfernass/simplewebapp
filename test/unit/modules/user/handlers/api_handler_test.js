const sinon = require('sinon');
const { expect } = require('chai');
const userHandler = require('../../../../../bin/modules/user/handlers/api_handler');
const commandHandler = require('../../../../../bin/modules/user/repositories/commands/command_handler');
const queryHandler = require('../../../../../bin/modules/user/repositories/queries/query_handler');
const jinbei = require('jinbei');

describe('User Api Handler', () => {

  let commandStub, logoutStub;

  const req = {
    body: {}
  };

  const res = {
    send: sinon.stub()
  };

  beforeEach(() => {
    commandStub = sinon.stub(commandHandler, 'loginUser');
    logoutStub = sinon.stub(commandHandler, 'logoutUser');
    commandStub.resolves({
      err: 'user not found',
      data: null
    });
  });

  afterEach(() => {
    commandStub.restore();
    logoutStub.restore();
  });

  describe('loginUser', () => {
    it('should cover error validation', async() => {
      await userHandler.loginUser(req, res);
    });
    it('should return user not found', async() => {
      sinon.stub(jinbei, 'isValidPayload').resolves({
        err: null,
        data: {}
      });
      await userHandler.loginUser(req, res);
      jinbei.isValidPayload.restore();
    });
    it('should return password invalid', async() => {
      sinon.stub(jinbei, 'isValidPayload').resolves({
        err: null,
        data: {}
      });
      commandStub.resolves({
        err: 'password invalid!',
        data: null
      });
      await userHandler.loginUser(req, res);
      jinbei.isValidPayload.restore();
    });
  });

  describe('logoutUser', () => {
    it('should cover error validation', async() => {
      await userHandler.logoutUser(req, res);
    });
    it('should return user logout success', async() => {
      req.body = {
        accessToken:'123456'
      };
      sinon.stub(jinbei, 'isValidPayload').resolves({
        err: null,
        data: {}
      });
      await userHandler.logoutUser(req, res);
      jinbei.isValidPayload.restore();
    });
    it('should return token invalid', async() => {
      sinon.stub(jinbei, 'isValidPayload').resolves({
        err: null,
        data: {}
      });
      logoutStub.resolves({
        err: 'token invalid!',
        data: null
      });
      await userHandler.logoutUser(req, res);
      jinbei.isValidPayload.restore();
    });
  });

  describe('getUser', () => {
    it('should return user data success', async() => {
      req.userId = '123456';
      sinon.stub(queryHandler, 'getUser').returns({
        err: null,
        message: 'success',
        data: {},
        code: 200
      });
      await userHandler.getUser(req, res);
      queryHandler.getUser.restore();
      expect(res.statusCode).to.not.equal(200);
    });
    it('should return data not found', async() => {
      sinon.stub(queryHandler, 'getUser').returns({
        err: {},
        message: 'success',
        data: null,
        code: 404
      });

      await userHandler.getUser(req, res);

      expect(res.statusCode).to.not.equal(404);
    });
  });

  describe('registerUser', () => {
    it('should return error validation', () => {
      userHandler.registerUser(req, res);
    });
    it('should return success', () => {
      sinon.stub(jinbei, 'isValidPayload').resolves({
        err: null,
        data: {}
      });
      sinon.stub(commandHandler, 'registerUser').resolves({
        err: null,
        data: {}
      });
      userHandler.registerUser(req, res);
      jinbei.isValidPayload.restore();
      commandHandler.registerUser.restore();
    });
  });

  describe('getRefreshToken', () => {
    it('should cover error validation', async() => {
      await userHandler.getRefreshToken(req, res);
    });
    it('should return token not valid', async() => {
      sinon.stub(jinbei, 'isValidPayload').resolves({
        err: null,
        data: {}
      });
      sinon.stub(commandHandler, 'getRefreshToken').resolves({
        err: {},
        data: null
      });
      await userHandler.getRefreshToken(req, res);
      jinbei.isValidPayload.restore();
      commandHandler.getRefreshToken.restore();
    });
    it('should return refresh token success', async() => {
      sinon.stub(jinbei, 'isValidPayload').resolves({
        err: null,
        data: {}
      });
      sinon.stub(commandHandler, 'getRefreshToken').resolves({
        err: null,
        data: {}
      });
      await userHandler.getRefreshToken(req, res);
      jinbei.isValidPayload.restore();
      commandHandler.getRefreshToken.restore();
    });
  });

  describe('verifyOtp', () => {
    it('should cover error validation', async() => {
      await userHandler.verifyOtp(req, res);
    });
    it('should return verify otp success', async() => {
      sinon.stub(jinbei, 'isValidPayload').resolves({
        err: null,
        data: {}
      });
      sinon.stub(commandHandler, 'verifyOtp').resolves({
        err: {},
        data: null
      });
      await userHandler.verifyOtp(req, res);
      jinbei.isValidPayload.restore();
      commandHandler.verifyOtp.restore();
    });
    it('should return verify otp failed', async() => {
      sinon.stub(jinbei, 'isValidPayload').resolves({
        err: null,
        data: {}
      });
      sinon.stub(commandHandler, 'verifyOtp').resolves({
        err: null,
        data: {}
      });
      await userHandler.verifyOtp(req, res);
      jinbei.isValidPayload.restore();
      commandHandler.verifyOtp.restore();
    });
  });
});
