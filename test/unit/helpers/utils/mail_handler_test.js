// FILEPATH: /Users/faridtriwicaksono/Documents/TELKOMSEL/DEV/assesment-project/user-service/test/unit/helpers/utils/mail_handler_test.js

const sinon = require('sinon');
const { expect } = require('chai');
const nodemailer = require('nodemailer');
const mailHandler = require('../../../../bin/helpers/utils/mail_handler');
const { ExpectationFailedError } = require('jinbei').Error;

describe('mailHandler', () => {
  let sendMailStub;


  beforeEach(() => {
    sendMailStub = sinon.stub(nodemailer, 'createTransport').returns({
      sendMail: sinon.stub().resolves('Email sent')
    });
  });

  afterEach(() => {
    sendMailStub.restore();
  });

  it('should send mail successfully', async () => {
    const mailOptions = {
      from: 'sender@example.com',
      to: 'receiver@example.com',
      subject: 'Test Subject',
      html: '<h1>Test HTML</h1>',
      text: '<h1>Test HTML</h1>'
    };
    const result = await mailHandler.send(mailOptions.html, mailOptions.to, mailOptions.subject);

    expect(result).to.be.an('object');
  });

  it('should log error and return error response when sending mail fails', async () => {
    sendMailStub.restore();
    sendMailStub = sinon.stub(nodemailer, 'createTransport').returns({
      sendMail: sinon.stub().rejects(new Error('Fail to send the message'))
    });

    const html = '<h1>Test HTML</h1>';
    const emailUser = 'receiver@example.com';
    const emailSubject = 'Test Subject';

    try {
      await mailHandler.send(html, emailUser, emailSubject);
    } catch (err) {
      expect(err).to.be.instanceOf(ExpectationFailedError);
      expect(err.message).to.equal('Fail to send the message');
    }
  });
});
