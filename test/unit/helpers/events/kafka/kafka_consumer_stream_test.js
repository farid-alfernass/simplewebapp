// FILEPATH: /Users/faridtriwicaksono/Documents/TELKOMSEL/DEV/assesment-project/user-service/test/unit/bin/helpers/kafka/kafka_consumer_stream.test.js

const chai = require('chai');
const sinon = require('sinon');
const kafka = require('kafka-node');
const ConsumerGroupStream = kafka.ConsumerGroupStream;
const KafkaConsumerStream = require('../../../../../bin/helpers/events/kafka/kafka_consumer_stream');

const expect = chai.expect;

describe('KafkaConsumerStream', () => {
  let sandbox;

  beforeEach(() => {
    sandbox = sinon.createSandbox();
  });

  afterEach(() => {
    sandbox.restore();
  });

  it('should correctly initialize', () => {
    const config = {
      groupId: 'test-group',
      topic: 'test-topic'
    };
    const kafkaConsumerStream = new KafkaConsumerStream(config);
    expect(kafkaConsumerStream).to.be.an.instanceOf(ConsumerGroupStream);
  });

});
