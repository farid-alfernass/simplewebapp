// FILEPATH: /Users/faridtriwicaksono/Documents/TELKOMSEL/DEV/assesment-project/user-service/test/unit/bin/helpers/events/kafka/kafka_producer.test.js

const sinon = require('sinon');
const proxyquire = require('proxyquire');
const jinbei = require('jinbei');

describe('KafkaProducer', () => {
  let sandbox;
  let producerStub;
  let logStub;
  let kafkaSendProducer;

  beforeEach(() => {
    sandbox = sinon.createSandbox();
    producerStub = {
      send: sandbox.stub(),
      on: sandbox.stub()
    };
    logStub = sandbox.stub(jinbei, 'log');
    const KafkaProducer = proxyquire('../../../../../bin/helpers/events/kafka/kafka_producer', {
      'kafka-node': {
        KafkaClient: sandbox.stub(),
        HighLevelProducer: sandbox.stub().returns(producerStub)
      }
    });
    kafkaSendProducer = KafkaProducer.kafkaSendProducer;
  });

  afterEach(() => {
    sandbox.restore();
  });

  it('should send data and log success', () => {
    const data = {
      body: { message: 'test' },
      topic: 'test-topic',
      attributes: 1,
      partition: 0
    };

    producerStub.send.yields(null, 'success');

    kafkaSendProducer(data);

    sinon.assert.calledWith(logStub, 'kafka-producer', 'Send data to "success"', 'Data has been send');
  });

  it('should log error when send fails', () => {
    const data = {
      body: { message: 'test' },
      topic: 'test-topic',
      attributes: 1,
      partition: 0
    };

    producerStub.send.yields('error', null);

    kafkaSendProducer(data);

    sinon.assert.calledWith(logStub, 'kafka-producer', 'error', 'producer-error-send');
  });
});
